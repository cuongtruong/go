package envutil

import "os"

// GetEnv returns environment variable by name
func GetEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}

	return fallback
}
