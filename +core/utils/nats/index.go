package natsutil

import (
	"encoding/json"
	"fmt"
	"os"
	"sync"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/google/uuid"
	"github.com/nats-io/nuid"
	stan "github.com/nats-io/stan.go"
)

// Message represents the payload sent over NATS pub/sub
type Message struct {
	ID      uuid.UUID   `json:"id"`
	State   string      `json:"state"`
	Payload interface{} `json:"payload"`
}

// StreamingComponent is containing reusable logic related to handling
// of the connection to NATS Streaming in the system.
type StreamingComponent struct {
	// logger is to log
	logger log.Logger

	// mtx is the mutual exclusion mtx from the component.
	mtx sync.Mutex

	// id is a unique identifier used for this component.
	id string

	// connection is the connection to NATS Streaming.
	connection stan.Conn

	// name is the name of component.
	name string
}

// NewStreamingComponent creates a StreamingComponent
func NewStreamingComponent(kind string) *StreamingComponent {
	id := nuid.Next()
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = level.NewFilter(logger, level.AllowDebug())
		logger = log.With(logger,
			"NATS Streaming", "Component",
			"timestamp:", log.DefaultTimestampUTC,
		)
	}

	return &StreamingComponent{
		id:     id,
		name:   kind,
		logger: logger,
	}
}

// ConnectToNATSStreaming connects to NATS Streaming
func (streamingComponent *StreamingComponent) ConnectToNATSStreaming(clusterID string, options ...stan.Option) error {
	streamingComponent.mtx.Lock()
	defer streamingComponent.mtx.Unlock()

	// Connect to NATS with Cluster Id, Client Id and customized options.
	connection, err := stan.Connect(clusterID, streamingComponent.id, options...)

	if err != nil {
		return err
	}

	streamingComponent.connection = connection

	return err
}

// NATS returns the current NATS connection.
func (streamingComponent *StreamingComponent) NATS() stan.Conn {
	streamingComponent.mtx.Lock()
	defer streamingComponent.mtx.Unlock()

	return streamingComponent.connection
}

// ID returns the ID from the component.
func (streamingComponent *StreamingComponent) ID() string {
	streamingComponent.mtx.Lock()
	defer streamingComponent.mtx.Unlock()

	return streamingComponent.id
}

// Name is the label used to identify the NATS connection.
func (streamingComponent *StreamingComponent) Name() string {
	streamingComponent.mtx.Lock()
	defer streamingComponent.mtx.Unlock()

	return fmt.Sprintf("%s:%s", streamingComponent.name, streamingComponent.id)
}

// Shutdown makes the component go away.
func (streamingComponent *StreamingComponent) Shutdown() error {
	streamingComponent.NATS().Close()

	streamingComponent.logger.Log("close-connection", streamingComponent.Name())

	return nil
}

// PublishMessage to NATS
func (streamingComponent *StreamingComponent) PublishMessage(subject string, data interface{}) error {
	dataJSON, _ := json.Marshal(data)
	sc := streamingComponent.NATS()
	err := sc.Publish(subject, dataJSON)

	if err != nil {
		level.Error(streamingComponent.logger).Log(
			"====================", "====================",
			"Publishing-message-failed: ", subject,
			"Error", err,
		)
	}

	level.Debug(streamingComponent.logger).Log(
		"====================", "====================",
		"Published-message: ", subject,
		"data", dataJSON,
	)

	return nil
}
