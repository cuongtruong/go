package orm

import (
	"time"

	"github.com/google/uuid"
	// "github.com/gofrs/uuid"
	// "gorm.io/gorm"
)

// Base model
type Base struct {
	ID        uuid.UUID  `json:"id" gorm:"<-:create;primaryKey;type:uuid;"`
	CreatedAt time.Time  `json:"createdAt" gorm:"<-:create"`
	UpdatedAt time.Time  `json:"updatedAt"`
	DeletedAt *time.Time `sql:"index" json:"deletedAt"`
}

// /**
//  * Hook to Auto generate id
//  *
//  * @param  {*gorm.DB} tx 				The GORM transaction
//  * @return {error} 							Success -> nil; Failed -> error
//  */
// func (model *Base) BeforeCreate(tx *gorm.DB) (err error) {
// 	uuid, err := uuid.NewV4()

// 	if err != nil {
// 		return err
// 	}

// 	model.ID = uuid

// 	return
// }
