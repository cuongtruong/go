package middleware

import "net/http"

/**
 * Set Header > Content-Type to json
 *
 * @param  {http.Handler} next 		The http handler
 * @return {http.Handler} 				The next http handler
 */
func ContentTypeJSONMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}
