package transport

import (
	"context"

	"github.com/go-kit/kit/endpoint"

	account "xxx/account"
)

// Endpoints for account
type Endpoints struct {
	Report     endpoint.Endpoint
	GetUsers   endpoint.Endpoint
	CreateUser endpoint.Endpoint
	GetUser    endpoint.Endpoint
	PutUser    endpoint.Endpoint
	DeleteUser endpoint.Endpoint
}

/**
 * Make account endpoints
 *
 * @param  {Service} service 	The service
 * @return {Endpoints} 				The endpoints
 */
func MakeEndpoints(service account.Service) Endpoints {
	return Endpoints{
		Report:     makeReportEndpoint(service),
		GetUsers:   makeGetUsersEndpoint(service),
		CreateUser: makeCreateUserEndpoint(service),
		GetUser:    makeGetUserEndpoint(service),
		PutUser:    makePutUserEndpoint(service),
		DeleteUser: makeDeleteUserEndpoint(service),
	}
}

/**
 * makeGetUsersEndpoint returns an endpoint via the passed service.
 *
 * @param  {Service} service 					The service
 * @return {endpoint.Endpoint} 				The endpoint
 */
func makeGetUsersEndpoint(service account.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(account.GetUsersRequest)
		res, err := service.GetUsers(ctx, req)

		return res, err
	}
}

/**
 * makeReportEndpoint returns an endpoint via the passed service.
 *
 * @param  {Service} service 					The service
 * @return {endpoint.Endpoint} 				The endpoint
 */
func makeReportEndpoint(service account.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		res, err := service.GetUsersCount(ctx)

		return res, err
	}
}

/**
 * makeCreateUserEndpoint returns an endpoint via the passed service.
 *
 * @param  {Service} service 					The service
 * @return {endpoint.Endpoint} 				The endpoint
 */
func makeCreateUserEndpoint(service account.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(account.CreateUserRequest)
		res, err := service.CreateUser(ctx, req)

		return res, err
	}
}

/**
 * makeGetUserEndpoint returns an endpoint via the passed service.
 *
 * @param  {Service} service 					The service
 * @return {endpoint.Endpoint} 				The endpoint
 */
func makeGetUserEndpoint(service account.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(account.GetUserRequest)
		res, err := service.GetUser(ctx, req.ID)

		return res, err
	}
}

/**
 * makePutUserEndpoint returns an endpoint via the passed service.
 *
 * @param  {Service} service 					The service
 * @return {endpoint.Endpoint} 				The endpoint
 */
func makePutUserEndpoint(service account.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(account.PutUserRequest)
		res, err := service.UpdateUser(ctx, req)

		return res, err
	}
}

/**
 * makeDeleteUserEndpoint returns an endpoint via the passed service.
 *
 * @param  {Service} service 					The service
 * @return {endpoint.Endpoint} 				The endpoint
 */
func makeDeleteUserEndpoint(service account.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(account.DeleteUserRequest)
		err := service.DeleteUser(ctx, req.ID)

		return nil, err
	}
}
