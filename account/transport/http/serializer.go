package http

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"

	errors "xxx/+core/errors"
	account "xxx/account"
)

func decodeGetUsersRequest(ctx context.Context, request *http.Request) (interface{}, error) {
	query := request.URL.Query()
	limit, _ := strconv.Atoi(query.Get("limit"))
	page, _ := strconv.Atoi(query.Get("page"))
	sortBy := query.Get("sortBy")
	search := query.Get("search")

	return account.GetUsersRequest{
		Limit:  limit,
		Page:   page,
		SortBy: sortBy,
		Search: search,
	}, nil
}

func decodeCreateUserRequest(ctx context.Context, request *http.Request) (interface{}, error) {
	var req account.CreateUserRequest

	err := json.NewDecoder(request.Body).Decode(&req)

	if err != nil {
		return nil, err
	}

	return req, nil
}

func decodeGetUserRequest(ctx context.Context, request *http.Request) (interface{}, error) {
	vars := mux.Vars(request)
	id, ok := vars["id"]

	if !ok {
		return nil, errors.ErrBadRouting
	}

	req := account.GetUserRequest{
		ID: id,
	}

	return req, nil
}

func decodePutUserRequest(ctx context.Context, request *http.Request) (interface{}, error) {
	vars := mux.Vars(request)
	id, ok := vars["id"]

	if !ok {
		return nil, errors.ErrBadRouting
	}

	var req account.PutUserRequest

	if err := json.NewDecoder(request.Body).Decode(&req); err != nil {
		return nil, err
	}

	req.ID = id

	return req, nil
}

func decodeDeleteUserRequest(ctx context.Context, request *http.Request) (interface{}, error) {
	vars := mux.Vars(request)
	id, ok := vars["id"]

	if !ok {
		return nil, errors.ErrBadRouting
	}

	req := account.DeleteUserRequest{
		ID: id,
	}

	return req, nil
}

func decodeReportRequest(ctx context.Context, request *http.Request) (interface{}, error) {
	return nil, nil
}
