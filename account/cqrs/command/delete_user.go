package command

import (
	"context"

	domain "xxx/account/domain"

	"github.com/google/uuid"
)

// DeleteUserCommand is to edit user
type DeleteUserCommand struct {
	TransactionID uuid.UUID
	ID            string
	User          domain.User
}

// DeleteUserCommandHandler is to handle DeleteUserCommand
type DeleteUserCommandHandler struct {
	repository domain.Repository
}

/**
 * NewDeleteUserCommandHandler initialization
 *
 * @param  {Repository} repo 								The repository
 * @return {DeleteUserCommandHandler} 			The command handler
 */
func NewDeleteUserCommandHandler(repo domain.Repository) DeleteUserCommandHandler {
	return DeleteUserCommandHandler{
		repository: repo,
	}
}

/**
 * Delete a user
 *
 * @param  {Context} ctx           					The context
 * @param  {string} id     									The user id
 * @return {(string, error)}       					Success: The deleted user id; Failed: The error message
 */
func (handler DeleteUserCommandHandler) Handle(ctx context.Context, id string) (string, error) {
	_, getUserErr := handler.repository.GetUser(ctx, id)

	if getUserErr != nil {
		return id, getUserErr
	}

	err := handler.repository.DeleteUser(ctx, id)

	return id, err
}
