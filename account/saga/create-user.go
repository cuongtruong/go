package saga

import (
	"context"
	"encoding/json"

	"github.com/go-kit/kit/log"
	"github.com/google/uuid"
	"github.com/nats-io/stan.go"

	"xxx/+core/utils/automapper"
	natsutil "xxx/+core/utils/nats"
	account "xxx/account"
	domain "xxx/account/domain"
)

const (
	CreateUserOrchestratorDurableID string = "create-user-orchestrator"
)

// CreateUserOrchestrator orchestrates the order processing task
type CreateUserOrchestrator struct {
	logger             log.Logger
	streamingComponent *natsutil.StreamingComponent
	service            account.Service
}

/**
 * NewCreateUserSaga
 *
 * @param  {Logger} logger 									The logger
 * @param  {Service} service 								The service
 * @param  {StreamingComponent} natsComp 		The NATS Streaming component
 * @return {Repository} 										The repository instance
 */
func NewCreateUserSaga(logger log.Logger, natsComp *natsutil.StreamingComponent, service account.Service) CreateUserOrchestrator {
	orchestrator := CreateUserOrchestrator{
		logger:             log.With(logger, "Context", "CreateUserOrchestrator", "streaming-component", natsComp.Name()),
		streamingComponent: natsComp,
		service:            service,
	}

	orchestrator.start()

	return orchestrator
}

// Start to orchestrate the process
func (orchestrator CreateUserOrchestrator) start() {
	go orchestrator.handleEvents()
}

// The orchestrator handles the user creation flow
func (orchestrator CreateUserOrchestrator) handleEvents() {
	sc := orchestrator.streamingComponent.NATS()

	for _, event := range []string{domain.UserCreatedEvent, domain.UserVerifiedEvent} {
		sc.QueueSubscribe(event, QueueGroup, func(msg *stan.Msg) {
			message := natsutil.Message{}
			err := json.Unmarshal(msg.Data, &message)

			orchestrator.logger.Log("===================================")

			if err != nil || message.Payload == nil || message.ID == uuid.Nil {
				orchestrator.logger.Log("Bad-message", msg)
				return
			}

			// Handle the message
			orchestrator.logger.Log("Raw-message", msg)

			switch msg.Subject {
			case domain.UserCreatedEvent:
				{
					orchestrator.handleUserCreated(message)
					break
				}
			case domain.UserVerifiedEvent:
				{
					orchestrator.handleUserVerified(message)
					break
				}
			}
		}, stan.DurableName(CreateUserOrchestratorDurableID))
	}
}

// Handle when user created
// - Increase counter
// - Verify user
func (orchestrator CreateUserOrchestrator) handleUserCreated(message natsutil.Message) error {
	orchestrator.streamingComponent.PublishMessage(domain.ActionIncreaseUserCounter, message)
	orchestrator.streamingComponent.PublishMessage(domain.ActionVerifyUser, message)

	return nil
}

// Handle when user verified
// - Activate user
func (orchestrator CreateUserOrchestrator) handleUserVerified(message natsutil.Message) error {
	ctx := context.Background()
	payload := message.Payload.(map[string]interface{})
	user, getUserErr := orchestrator.service.GetUser(ctx, payload["id"].(string))

	if getUserErr != nil {
		return getUserErr
	}

	request := account.PutUserRequest{}
	automapper.Map(&user, &request)
	request.Active = true

	// Set activate user
	_, updateUserErr := orchestrator.service.UpdateUser(ctx, request)

	// Just a demonstration...to rollback distributed transactions
	if updateUserErr != nil {
		orchestrator.streamingComponent.PublishMessage(domain.ActionRollbackUserCreation, message)

		return updateUserErr
	}

	orchestrator.logger.Log("User-creation-flow-completed", "===================================")

	return nil
}
