package account

// Events
const (
	// UserCreatedEvent -> User created
	UserCreatedEvent = "UserCreatedEvent"

	// UserUpdatedEvent -> User updated
	UserUpdatedEvent = "UserUpdatedEvent"

	// UserDeletedEvent -> User deleted
	UserDeletedEvent = "UserDeletedEvent"

	// UserVerifiedEvent -> User verified
	UserVerifiedEvent = "UserVerifiedEvent"
)
