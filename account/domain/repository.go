package account

import (
	"context"
	"sync"

	"github.com/go-kit/kit/log"
	"gorm.io/gorm"

	errors "xxx/+core/errors"
	orm "xxx/+core/orm"
)

// Repository to handle user stuff
type Repository interface {
	/**
	 * Get users
	 *
	 * @param  {context.Context} ctx 		The context
	 * @param  {UserFilter} ctx 				The filters
	 * @return {(Paginator, error)} 		Success -> paginator info; Failed -> error
	 */
	GetUsers(ctx context.Context, filter UserFilter) (orm.Paginator, error)

	/**
	 * Get users count
	 *
	 * @param  {context.Context} ctx 		The context
	 * @return {(int64, error)} 				Success -> users count; Failed -> error
	 */
	GetUsersCount(ctx context.Context) (int64, error)

	/**
	 * Create new user
	 *
	 * @param  {context.Context} ctx 		The context
	 * @param  {User} user 							The user
	 * @return {(User, error)} 					Success -> user; Failed -> error
	 */
	CreateUser(ctx context.Context, user User) (User, error)

	/**
	 * Get user by id
	 *
	 * @param  {context.Context} ctx 		The context
	 * @param  {string} id 							The user id
	 * @return {(User, error)} 					Success -> user; Failed -> error
	 */
	GetUser(ctx context.Context, id string) (User, error)

	/**
	 * Update user info
	 *
	 * @param  {context.Context} ctx 		The context
	 * @param  {User} user 							The user info
	 * @return {(User, error)} 					Success -> user; Failed -> error
	 */
	UpdateUser(ctx context.Context, user User) (User, error)

	/**
	 * Delete user
	 *
	 * @param  {context.Context} ctx 		The context
	 * @param  {string} id 							The user id
	 * @return {(error)} 								Success -> nil; Failed -> error
	 */
	DeleteUser(ctx context.Context, id string) error
}

////////////////////////////////////////////////////////////////////////////////////////////
// Implementation
////////////////////////////////////////////////////////////////////////////////////////////

type repository struct {
	db     *gorm.DB
	logger log.Logger
	mtx    sync.RWMutex
}

/**
 * NewRepository
 *
 * @param  {*sql.DB} db 					The db
 * @param  {log.Logger} logger 		The logger
 * @return {Repository} 					The repository instance
 */
func NewRepository(db *gorm.DB, logger log.Logger) Repository {
	return &repository{
		db:     db,
		logger: log.With(logger, "repository", "sql"),
	}
}

/**
 * Get users
 *
 * @param  {context.Context} ctx 		The context
 * @param  {UserFilter} ctx 				The filters
 * @return {(Paginator, error)} 		Success -> paginator info; Failed -> error
 */
func (repo *repository) GetUsers(ctx context.Context, filter UserFilter) (orm.Paginator, error) {
	repo.mtx.Lock()
	defer repo.mtx.Unlock()

	db := repo.db
	var users []User
	var orderBy string

	// Order by
	if len(filter.SortBy) > 0 {
		orderBy = filter.SortBy
	} else {
		orderBy = "updated_at DESC"
	}

	// Search by name/email
	if len(filter.Search) > 0 {
		searchBy := "%" + filter.Search + "%"
		db = db.Where("name ILIKE ?", searchBy).Or("email ILIKE ?", searchBy)
	}

	db = db.Order(orderBy)
	paginator, err := orm.Paging(&orm.PagingParam{
		DB:      db,
		Page:    filter.Page,
		Limit:   filter.Limit,
		ShowSQL: true,
	}, &users)
	paginator.Records = users

	return paginator, err
}

/**
 * Get users count
 *
 * @param  {context.Context} ctx 		The context
 * @return {(int64, error)} 				Success -> users count; Failed -> error
 */
func (repo *repository) GetUsersCount(ctx context.Context) (int64, error) {
	repo.mtx.Lock()
	defer repo.mtx.Unlock()

	var counter UserCounter
	result := repo.db.FirstOrCreate(&counter)

	return counter.All, result.Error
}

/**
 * Create new user
 *
 * @param  {context.Context} ctx 		The context
 * @param  {User} user 							The user
 * @return {(User, error)} 					Success -> user; Failed -> error
 */
func (repo *repository) CreateUser(ctx context.Context, user User) (User, error) {
	repo.mtx.Lock()
	defer repo.mtx.Unlock()

	// TODO: validation
	if user.Email == "" || user.Name == "" {
		return user, errors.ErrBadRequest
	}

	result := repo.db.Create(&user)

	if result.Error != nil {
		return user, result.Error
	}

	return user, nil
}

/**
 * Get user by id
 *
 * @param  {context.Context} ctx 		The context
 * @param  {string} id 							The user id
 * @return {(User, error)} 					Success -> user; Failed -> error
 */
func (repo *repository) GetUser(ctx context.Context, id string) (User, error) {
	repo.mtx.Lock()
	defer repo.mtx.Unlock()

	var user User

	result := repo.db.First(&user, "id = ?", id)

	if result.Error == gorm.ErrRecordNotFound {
		return user, errors.ErrDataNotFound
	}

	return user, result.Error
}

/**
 * Update user info
 *
 * @param  {context.Context} ctx 		The context
 * @param  {User} user 							The user info
 * @return {(User, error)} 					Success -> user; Failed -> error
 */
func (repo *repository) UpdateUser(ctx context.Context, user User) (User, error) {
	repo.mtx.Lock()
	defer repo.mtx.Unlock()

	result := repo.db.Select("*").Updates(user)

	if result.Error == gorm.ErrRecordNotFound {
		return user, errors.ErrDataNotFound
	}

	return user, result.Error
}

/**
 * Delete user
 *
 * @param  {context.Context} ctx 		The context
 * @param  {string} id 							The user id
 * @return {(error)} 								Success -> nil; Failed -> error
 */
func (repo *repository) DeleteUser(ctx context.Context, id string) error {
	repo.mtx.Lock()
	defer repo.mtx.Unlock()

	result := repo.db.Where("id = ?", id).Delete(&User{})

	return result.Error
}
